//
//  EventViewController.swift
//  Bee Circle
//
//  Created by Caroline Ignacio on 25/05/2019.
//  Copyright © 2019 HiveBEEngers. All rights reserved.
//

import UIKit
import  CoreLocation

class EventViewController: UIViewController {

    @IBOutlet weak var dtTo: UIDatePicker!
    @IBOutlet weak var dtFrom: UIDatePicker!
    @IBOutlet weak var txtLocation: UITextView!
    @IBOutlet weak var lblTitle: UITextField!
    @IBOutlet weak var txtDesc: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        UI()
        
    }
    @IBAction func didTapSubmit(_ sender: UIButton) {
        getLoc()
        getTime()

    }
    func getTime(){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyy hh:mm"
        
        self.dtFrom.datePickerMode = .dateAndTime
        let dateFromString = dateFormatter.string(from: self.dtFrom.date)
        print("date from:", dateFromString)
        
        self.dtTo.datePickerMode = .dateAndTime
        let dateToString = dateFormatter.string(from: self.dtTo.date)
        print("date to:", dateToString)
        
    }
    func getLoc(){
        let geoCoder = CLGeocoder()
        let address = txtLocation.text

        geoCoder.geocodeAddressString(address ?? "Unavailable address") { (placemarks, error) in
            guard
                let placemarks = placemarks,
                let location = placemarks.first?.location
                else {
                    // handle no location found
                    print("no loc")
                    return
            }
            let loc = location.coordinate
            print("coord:",loc)
            let long = loc.longitude
            let lat = loc.latitude
            
        }

    }
    func UI(){
        //
        self.txtDesc.layer.borderColor = UIColor.lightGray.cgColor
        self.txtDesc.layer.cornerRadius = 5
        self.txtDesc.layer.borderWidth = 0.5
        
        self.txtLocation.layer.borderColor = UIColor.lightGray.cgColor
        self.txtLocation.layer.cornerRadius = 5
        self.txtLocation.layer.borderWidth = 0.5
        
        ///////////////////////////////////////////////////////
        navigationController?.navigationBar.isTranslucent = false
        let attrs = [
            NSAttributedString.Key.foregroundColor: Constants.tintColor,
            NSAttributedString.Key.font: UIFont(name: "SFProDisplay-Bold", size: 32)!
        ]
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.largeTitleTextAttributes = attrs
        navigationController?.navigationBar.titleTextAttributes = attrs
        navigationController?.navigationBar.barTintColor = Constants.baseColor;        navigationController?.navigationBar.topItem?.title = "Event Registration"
    }
    

}
