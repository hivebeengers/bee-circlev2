//
//  BaseNavigationViewController.swift
//  Bee Circle
//
//  Created by Caroline Ignacio on 25/05/2019.
//  Copyright © 2019 HiveBEEngers. All rights reserved.
//

import UIKit
class BaseNavigationViewController: UITabBarController, UITabBarControllerDelegate {
    
    var navDiscussion = UINavigationController()
    var navNotification = UINavigationController()
    var navMore = UINavigationController()
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        setUpNavBar()
        self.viewControllers = [navDiscussion , navNotification, navMore]
        navigationController?.navigationBar.isHidden = true
    }
    // UITabBarControllerDelegate method
    func tabBarController(_ tabBarController: UITabBarController, didSelectdidSelect viewController: UIViewController) {
        print("SelectedBar \(viewController.title!)")
    }
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        return viewController != tabBarController.selectedViewController
    }
    
    func setUpNavBar(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let discussion = storyboard.instantiateViewController(withIdentifier: "DiscussionsViewController")  as! DiscussionsViewController
        discussion.tabBarItem = UITabBarItem(
            title: "Events",
            image:  UIImage(named: "tab-discussion"),
            tag: 1)
        navDiscussion = UINavigationController.init(rootViewController: discussion)
        setUpNavigationBar(navigationController: navDiscussion)
        
    
        
        let notif = storyboard.instantiateViewController(withIdentifier: "NotificationViewController")  as! NotificationViewController
        notif.tabBarItem = UITabBarItem(
            title: "Notification",
            image:  UIImage(named: "tab-notification"),
            tag: 3)
        navNotification = UINavigationController.init(rootViewController: notif)
        setUpNavigationBar(navigationController: navNotification)
        
        let moree = storyboard.instantiateViewController(withIdentifier: "MoreViewController")  as! MoreViewController
        moree.tabBarItem = UITabBarItem(
            title: "More",
            image:  UIImage(named: "tab-more"),
            tag: 4)
        navMore = UINavigationController.init(rootViewController: moree)
        setUpNavigationBar(navigationController: navMore)
        
        
        }
    
    func setUpNavigationBar(navigationController: UINavigationController) -> Void{
        UITabBar.appearance().tintColor = Constants.baseColor
        self.tabBar.unselectedItemTintColor = Constants.tintColor
        
        navigationController.navigationBar.isTranslucent = false
        let attrs = [
            NSAttributedString.Key.foregroundColor: Constants.tintColor,
            NSAttributedString.Key.font: UIFont(name: "SFProDisplay-Bold", size: 32)!
        ]
        navigationController.navigationBar.prefersLargeTitles = true
        navigationController.navigationBar.largeTitleTextAttributes = attrs
        navigationController.navigationBar.titleTextAttributes = attrs
        navigationController.navigationBar.barTintColor = Constants.baseColor;        navigationController.navigationBar.topItem?.title = "Bee Circle"
        
        let backbutton = UIImage(named: "backbutton")
        navigationController.navigationBar.backIndicatorImage = backbutton
        navigationController.navigationBar.backIndicatorTransitionMaskImage = backbutton
        navigationController.navigationBar.tintColor = .white
        navigationController.editButtonItem.title = ""
    }
    override open var shouldAutorotate: Bool {
        return false
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    

}
