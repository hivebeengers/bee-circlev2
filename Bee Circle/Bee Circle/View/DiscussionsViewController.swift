//
//  DiscussionsViewController.swift
//  Bee Circle
//
//  Created by Caroline Ignacio on 25/05/2019.
//  Copyright © 2019 HiveBEEngers. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class DiscussionsViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var categories = ["Event Near Me", "More Event"]
    var eventsArr = [[String:AnyObject]]()
    var isAPIcalled : Bool = false
    
    // MARK: Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getEvents()
    }
    
    // MARK: API calls
    func getEvents() {
        
        Alamofire.request("https://hivebeengers.outsystemscloud.com/BeeCircle/rest/EventList/getEvent").responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                //let eventJson = JSON(responseData.result.value!)
                //self.eventsArr = eventJson["result"].array!
                
                let swiftyJsonVar = JSON(responseData.result.value!)
                
                if let resData = swiftyJsonVar["result"].arrayObject {
                    self.eventsArr = resData as! [[String:AnyObject]]
                }
                
                self.isAPIcalled = true
                if self.eventsArr.count > 0 {
                    self.tableView.reloadData()
                }
            }
        }
    }
}

// MARK: Extensions
extension DiscussionsViewController : UITabBarDelegate{
    
}

extension DiscussionsViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return categories[section]
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return categories.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 256;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryRow") as! CategoryRowTableViewCell
        
        // Pass array to cell
        if isAPIcalled {
            cell.eventsArr = self.eventsArr
            cell.collectionView .reloadData()
        }
        
        
        return cell
    }
    
    
}
