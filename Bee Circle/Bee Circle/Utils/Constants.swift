//
//  File.swift
//  Bee Circle
//
//  Created by Caroline Ignacio on 25/05/2019.
//  Copyright © 2019 HiveBEEngers. All rights reserved.
//

import Foundation
import UIKit
struct Constants{
    static let requiredUserNameLength = 5
    static let requiredMobileNumberLength = 11
    static let notificationName = Notification.Name("docID")
    static let baseColor = UIColor(red:0.98, green:0.85, blue:0.29, alpha:1.0)
    static let tintColor = UIColor(red:0.29, green:0.13, blue:0.04, alpha:1.0)
    struct Endpoints {
    }
    
    struct Routes{
    }
}
