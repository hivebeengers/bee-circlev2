//
//  LoginViewController.swift
//  Bee Circle
//
//  Created by Jamie Aguinaldo on 5/25/19.
//  Copyright © 2019 HiveBEEngers. All rights reserved.
//

import UIKit
import Alamofire
import MaterialComponents.MaterialSnackbar
import SkyFloatingLabelTextField
import SwiftyJSON

class LoginViewController: UIViewController {
    
    // Views
    @IBOutlet weak var txtFreeBeePhoneNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var btnLogin: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initViews()
    }

    // MARK: API calls
    func getFreeBeeToken() {
        // TODO: Concatenate baseURL instead to endpoint
    
        // Initialize headers
        //let consumerKey = "c6TfI86oaalEw1f_VXrXnDlYfIMa"
        //let consumerSecret = "bpPyK_nHAARJaciPJiIMBHi48ega"
        //let consumerKeyAndSecret = "\(consumerKey):\(consumerSecret)"
        
        // TODO: Convert into base64
        let headers: HTTPHeaders = [
            "authorization" : "Basic YzZUZkk4Nm9hYWxFdzFmX1ZYclhuRGxZZklNYTpicFB5S19uSEFBUkphY2lQSmlJTUJIaTQ4ZWdh",
            ]
        let params : Parameters = ["grant_type":"client_credentials"]
        
        Alamofire.request("https://techdata.smart.com.ph/token",
                          method: .post,
                          parameters: params,
                          encoding: URLEncoding.httpBody,
                          headers: headers).responseJSON { (responseData) -> Void in
                            if((responseData.result.value) != nil) {
                                let accessToken = JSON(responseData.result.value!)["access_token"].string
                                self.getFreeBeeUserStatus(bearerToken: accessToken!)
                            }
        }
    }
    
    func getFreeBeeUserStatus(bearerToken: String) {
        let headers: HTTPHeaders = [
            "authorization" : "Bearer \(bearerToken)",
            ]
        
        Alamofire.request("https://techdata.smart.com.ph/sandbox/t/pldtglobal.com/hackathon-api/1.0.0/freebee/check-account/\(self.txtFreeBeePhoneNumber.text!)",
            headers: headers).responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let freeBeeUserStatus = JSON(responseData.result.value!)["data"]["status"].string
                    
//                    let message = MDCSnackbarMessage()
//                    message.text = freeBeeUserStatus
////                    MDCSnackbarManager.show(message)
                    
                    self.performSegue(withIdentifier: "goToHome", sender: nil)
                }
                
        }
    }
    
    // MARK: Views
    func initViews() {
        self.txtFreeBeePhoneNumber.font = UIFont(name: "SFProDisplay-Regular", size: 20)
        self.btnLogin.titleLabel?.font = UIFont(name: "SFProDisplay-Bold", size: 20)
    }

    // MARK: Events
    @IBAction func loginUser(_ sender: Any) {
        getFreeBeeToken()
    }
}
