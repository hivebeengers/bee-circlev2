//
//  CategoryRowTableViewCell.swift
//  Bee Circle
//
//  Created by Caroline Ignacio on 25/05/2019.
//  Copyright © 2019 HiveBEEngers. All rights reserved.
//

import UIKit
import SwiftyJSON

class CategoryRowTableViewCell: UITableViewCell {
    
    var eventsArr = [Any]()
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
extension CategoryRowTableViewCell: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ContentCollection", for: indexPath) as! ContentCollectionViewCell
        
        if self.eventsArr.count != 0 {
            let eventDict = self.eventsArr[indexPath.row] as? [String:Any]
            
            cell.imgCover?.image = UIImage(imageLiteralResourceName: "coverimg")
            cell.lblTitle.text = eventDict!["event_title"] as? String
            cell.txtDescription.text = eventDict!["event_description"] as? String
        }
        
        return cell
    }
}
extension CategoryRowTableViewCell: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemsPerRow:CGFloat = 2
        let hardCodedPadding:CGFloat = 3
        let itemWidth = (collectionView.bounds.width / itemsPerRow) - hardCodedPadding
        let itemHeight = collectionView.bounds.height - (2 * hardCodedPadding)
        return CGSize(width: itemWidth, height: itemHeight)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print("tap:", indexPath)
    }
}
