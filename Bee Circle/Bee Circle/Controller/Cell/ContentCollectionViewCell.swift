//
//  ContentCollectionViewCell.swift
//  Bee Circle
//
//  Created by Caroline Ignacio on 25/05/2019.
//  Copyright © 2019 HiveBEEngers. All rights reserved.
//

import UIKit

class ContentCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgCover: UIImageView?
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtDescription: UITextView!
}
